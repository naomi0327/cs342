//
//  AddTaskViewController.swift
//  test
//
//  Created by Naomi Yamamoto on 4/19/15.
//  Copyright (c) 2015 Naomi & Phuong. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class AddTaskViewController: UIViewController{
   
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.ListTableView?.delegate = self
        //self.ListTableView?.dataSource = self
        // Do any additional setup after loading the view, typically from a nib.
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBOutlet weak var taskNameField: UITextField!
    
    //If you click on Cancel button on the app, it goes back to previous view, Task List 
    @IBAction func goBackToTaskMain(sender: AnyObject) {
        navigationController?.popViewControllerAnimated(true)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        var appDel : AppDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
        var context : NSManagedObjectContext = appDel.managedObjectContext!
        
        var newTask = NSEntityDescription.insertNewObjectForEntityForName("Task", inManagedObjectContext: context) as! NSManagedObject
        
        newTask.setValue(taskNameField.text as String, forKey: "taskName")
        
        var error : NSError?
        context.save(&error)
        
        println(newTask)
        println("Object saved")
        
    }
    
}
