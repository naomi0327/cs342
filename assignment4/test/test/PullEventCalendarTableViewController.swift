//
//  PullEventCalendar.swift
//  test
//
//  Created by Phuong on 4/19/15.
//  Copyright (c) 2015 Naomi & Phuong. All rights reserved.
//

import UIKit
import EventKit
import EventKitUI
import AVFoundation

class PullEventCalendar: UITableViewController, AVAudioPlayerDelegate {
    var database = EKEventStore()
    var audioPlayer: AVAudioPlayer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Get path for audio file and Create an audio player
        let audioPath = NSURL(fileURLWithPath: NSBundle.mainBundle().pathForResource("Pachelbel-Cannon", ofType: "mp3")!)
        var audioError:NSError?
        audioPlayer = AVAudioPlayer(contentsOfURL: audioPath, error:&audioError)
        if let error = audioError {
            println("Error \(error.localizedDescription)")
        }
        //Play audio file
        audioPlayer!.delegate = self
        audioPlayer!.play()
        
        self.determineStatus()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "determineStatus", name: UIApplicationWillEnterForegroundNotification, object: nil)
        

        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let all_calendar = self.database.calendarsForEntityType(EKEntityTypeEvent)
        
        //today
        let today = NSDate()
        //yesterday
        let next = NSDate()
        let tmr = next.dateByAddingTimeInterval(3600*24*7)
        
        let fetchCalendarEvent =
        self.database.predicateForEventsWithStartDate(today, endDate: tmr, calendars:
            all_calendar)
        
        let eventlist = self.database.eventsMatchingPredicate(fetchCalendarEvent)
        // Do any additional setup after loading the view.
        if eventlist != nil {
            return eventlist.count
        }else{
            return 0
        }
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath)
        -> UITableViewCell {
            let all_calendar = self.database.calendarsForEntityType(EKEntityTypeEvent)
            
            //today
            let today = NSDate()
            //yesterday
            let next = NSDate()
            let tmr = next.dateByAddingTimeInterval(3600*24*7)
            
            let fetchCalendarEvent =
            self.database.predicateForEventsWithStartDate(today, endDate: tmr, calendars:
                all_calendar)
            
            let eventlist = self.database.eventsMatchingPredicate(fetchCalendarEvent)
            // Do any additional setup after loading the view.
            let cell = tableView.dequeueReusableCellWithIdentifier("events", forIndexPath: indexPath)
                as! UITableViewCell
            
            let event = eventlist[indexPath.row]
            cell.textLabel?.text = event.title!!

            if var eventNote = event.notes! {
                cell.detailTextLabel?.text = "Start Date: \(String(stringInterpolationSegment: event.startDate)) -- Notes: \(String(stringInterpolation: event.notes))"
            }else {
                cell.detailTextLabel?.text = "Start Date: \(String(stringInterpolationSegment: event.startDate))"
            }
            return cell
    }
    
    @IBAction func pullAllEvent (sender:AnyObject!){
        
        if !self.determineStatus() {
            println("not authorized")
            return
        }
        
        let all_calendar = self.database.calendarsForEntityType(EKEntityTypeEvent)
        
        //today
        let today = NSDate()
        //yesterday
        let next = NSDate()
        let tmr = next.dateByAddingTimeInterval(3600*24*7)
        
        let fetchCalendarEvent =
        self.database.predicateForEventsWithStartDate(today, endDate: tmr, calendars:
            all_calendar)
        
        let eventlist = self.database.eventsMatchingPredicate(fetchCalendarEvent)
        
        for event in eventlist{
            println(event.title!!)
            println(event.notes)
            println(event.startDate)
        }
    }
    
    func determineStatus() -> Bool {
        let type = EKEntityTypeEvent
        let stat = EKEventStore.authorizationStatusForEntityType(type)
        switch stat {
        case .Authorized:
            return true
        case .NotDetermined:
            self.database.requestAccessToEntityType(type, completion:{_,_ in})
            return false
        case .Restricted:
            return false
        case .Denied:
            let alert = UIAlertController(title: "Need Authorization", message: "Please let our app access your calendar", preferredStyle: .Alert)
            alert.addAction(UIAlertAction(title: "No", style: .Cancel, handler: nil))
            alert.addAction(UIAlertAction(title: "OK!", style: .Default, handler: {
                _ in
                let url = NSURL(string:UIApplicationOpenSettingsURLString)!
                UIApplication.sharedApplication().openURL(url)
            }))
            self.presentViewController(alert, animated:true, completion:nil)
            return false
        }
    }
    
    
}
