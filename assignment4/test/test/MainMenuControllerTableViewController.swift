//
//  MainMenuControllerTableViewController.swift
//  test
//
//  Created by Mac on 4/15/15.
//  Copyright (c) 2015 Naomi Yamamoto. All rights reserved.
//

import UIKit
import CoreData

class MainMenuControllerTableViewController: UITableViewController {
    let tasks = ["Add Event to Calendar",  "View 7 day events","Task List"]
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return 3
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCellWithIdentifier("Task1", forIndexPath: indexPath) as! UITableViewCell
            cell.textLabel?.text = self.tasks[indexPath.row]
            var imageName = UIImage(named: "Calendar.png")
            cell.imageView!.image = imageName
            return cell
        }else if indexPath.row == 1 {
            let cell = tableView.dequeueReusableCellWithIdentifier("Task2", forIndexPath: indexPath) as! UITableViewCell
            cell.textLabel?.text = self.tasks[indexPath.row]
            var imageName = UIImage(named: "Music.png")
            cell.imageView!.image = imageName
            return cell
        }else{
            let cell = tableView.dequeueReusableCellWithIdentifier("Task3", forIndexPath: indexPath) as! UITableViewCell
            cell.textLabel?.text = self.tasks[indexPath.row]
            var imageName = UIImage(named: "Cow.png")
            cell.imageView!.image = imageName
            return cell
        }

        // Configure the cell...

    }

}
