//
//  TaskMainViewController.swift
//  test
//
//  Created by Naomi Yamamoto on 4/20/15.
//  Copyright (c) 2015 Naomi & Phuong. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class TaskMainViewController: UIViewController{
    var taskList = [String?]()
    let managedObjectContext = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
    var fetchedResultController: NSFetchedResultsController = NSFetchedResultsController()
    
    @IBOutlet weak var taskTableView: UITableView!
    
    //Populate FetchedResultController
    func getFetchedResultController() -> NSFetchedResultsController {
        fetchedResultController = NSFetchedResultsController(fetchRequest: self.todoFetchRequest(), managedObjectContext: managedObjectContext!, sectionNameKeyPath: nil, cacheName: nil)
        return fetchedResultController
    }
    
    func todoFetchRequest() -> NSFetchRequest {
        let fetchRequest = NSFetchRequest(entityName: "Task")
        let sortDescriptor = NSSortDescriptor(key: "taskName", ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor]
        return fetchRequest
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        fetchedResultController = self.getFetchedResultController()
        fetchedResultController.performFetch(nil)
        
        loadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func unwindToTaskMainViewController(segue: UIStoryboardSegue) {
        
        taskList.removeAll(keepCapacity: false)
        loadData()
        taskTableView.reloadData()
    }
    
    func loadData(){
        println("loading data... please wait...")
        var appDel = UIApplication.sharedApplication().delegate as! AppDelegate
        var context = appDel.managedObjectContext
        var error : NSError?
        
        var request = NSFetchRequest(entityName: "Task")
        request.returnsObjectsAsFaults = false
        
        var results : NSArray = context!.executeFetchRequest(request, error: &error)! as NSArray
        if results.count > 0 {
            for res in results{
                taskList += [res.valueForKey("taskName") as! String?]
            }
        }else{
            println("no data loaded")
        }
    }
    
    //Following functions are to display the list of tasks added
    func numberOfSectionsInTableView(taskTableView:UITableView!)->Int
    {
        return 1
    }
    
    func tableView(taskTableView: UITableView!, numberOfRowsInSection section: Int) -> Int
    {
        return taskList.count;
    }
    
    func tableView(taskTableView: UITableView!, cellForRowAtIndexPath indexPath: NSIndexPath!) -> UITableViewCell!
    {
        let cell:UITableViewCell = UITableViewCell(style:UITableViewCellStyle.Default, reuseIdentifier:"cell")
        cell.textLabel?.text = taskList[indexPath.row]
        
        return cell
    }
    
    func tableView(taskTableView: UITableView!, didSelectRowAtIndexPath indexPath: NSIndexPath!)
    {
        println("You selected cell #\(indexPath.row)!")
    }
    
    //Followings are the functions to handle delete actions
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
            if editingStyle == UITableViewCellEditingStyle.Delete {
                taskList.removeAtIndex(indexPath.row)
                tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Automatic)
                
                let managedObject: NSManagedObject = fetchedResultController.objectAtIndexPath(indexPath) as! NSManagedObject
                managedObjectContext?.deleteObject(managedObject)
                managedObjectContext?.save(nil)
                
    
         }
    }
    
    func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    
    func tableView(tableView: UITableView, moveRowAtIndexPath sourceIndexPath: NSIndexPath, toIndexPath destinationIndexPath: NSIndexPath) {
        let tmp = taskList[sourceIndexPath.row]
        taskList.removeAtIndex(sourceIndexPath.row)
        taskList.insert(tmp, atIndex: destinationIndexPath.row)
    }
    
}