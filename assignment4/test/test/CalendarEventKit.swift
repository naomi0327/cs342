/* CalendarEventKit
* Create Event in Calendar
* Created by Phuong on 4/17/15.
* Copyright (c) 2015 Naomi & Phuong. All rights reserved.
*/

import UIKit
import EventKit
import EventKitUI

class CalendarEventKit: UIViewController, EKCalendarChooserDelegate {
    
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var eventName: UITextField!
    @IBOutlet weak var eventNote: UITextField!
    @IBOutlet weak var datePicker: UIDatePicker!

    var database = EKEventStore()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.determineStatus()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "determineStatus", name: UIApplicationWillEnterForegroundNotification, object: nil)
    }

    @IBAction func createEvent (sender:AnyObject!) {
        //Button click -> create event
        
        var eventNametext = "Unknown"
        println(eventName.text)
        if (eventName.text != "") {
            eventNametext = eventName.text}
        
        if !self.determineStatus() {      //try permission
            println("not authorized")
            return
        }
        
        let cal : EKCalendar! = self.calendarWithName("Phuong-Calendar")
        if cal == nil {
            return
        }
        
        let calendar = NSCalendar.currentCalendar()
        let components = calendar.components(.CalendarUnitYear | .CalendarUnitMonth | .CalendarUnitDay | .CalendarUnitHour | .CalendarUnitMinute, fromDate: datePicker.date)
        //read date
        
        let comp = NSDateComponents()
        comp.year = components.year
        comp.month = components.month
        comp.day = components.day
        comp.hour = components.hour
        comp.minute = components.minute
        let d1 = calendar.dateFromComponents(comp)
        comp.hour = comp.hour + 1
        let d2 = calendar.dateFromComponents(comp)
        //Set event to be sent into calendar
        let event = EKEvent(eventStore:self.database)
        event.title = eventNametext
        event.notes = eventNote.text
        event.calendar = cal
        event.startDate = d1
        event.endDate = d2
        
        // add an alarm notification 1 min before event
        let alarm = EKAlarm(relativeOffset:-60) // 60 seconds before
        event.addAlarm(alarm)
        
        var err : NSError?
        let ok = self.database.saveEvent(event, span:EKSpanThisEvent, commit:true, error:&err)
        
        if !ok {
            println("ERROOR! \(err!.localizedDescription)")
            return
        }
        
        //print out status for tracking
        let Label = "Status: Add event \'\(eventNametext)\' at \n \(String(components.year))/\(String(components.month))/\(String(components.day)) \(String(components.hour)):\(String(components.minute)). Event lasts till \(String(comp.hour)):\(String(components.minute)) (1 hour). \nNotification alarm 1 min before event. \n Check changes in the simulator view's calendar (top panel)"
        //let Label = "Status: Set event at \(Printdate)"//in +GST Time zone
        dateLabel.text = Label
        
    }

    
    func determineStatus() -> Bool {
        //Check if the app has permission to use Calendar
        let type = EKEntityTypeEvent
        let stat = EKEventStore.authorizationStatusForEntityType(type)
        switch stat {
        case .Authorized:
            return true
        case .NotDetermined:
            self.database.requestAccessToEntityType(type, completion:{_,_ in})
            return false
        case .Restricted:
            return false
        case .Denied:
            let alert = UIAlertController(title: "Need Authorization", message: "Please let our app access your calendar", preferredStyle: .Alert)
            alert.addAction(UIAlertAction(title: "No", style: .Cancel, handler: nil))
            alert.addAction(UIAlertAction(title: "OK!", style: .Default, handler: {
                _ in
                let url = NSURL(string:UIApplicationOpenSettingsURLString)!
                UIApplication.sharedApplication().openURL(url)
            }))
            self.presentViewController(alert, animated:true, completion:nil)
            return false
        }
    }
    
    func createCalendar () {
        //Create a new calendar if No calendar exists yet
        var src : EKSource! = nil
        for source in self.database.sources() as! [EKSource] {
            if source.sourceType.value == EKSourceTypeLocal.value {
                src = source
                break
            }
        }
        if src == nil {
            println("failed to find local source")
            return
        }
        let cal = EKCalendar(forEntityType:EKEntityTypeEvent,
            eventStore:self.database)
        cal.source = src
        cal.title = "Phuong-Calendar"
        // ready to save the new calendar into the database!
        var err : NSError?
        let ok = self.database.saveCalendar(cal, commit:true, error:&err)
        if !ok {
            println("save calendar error: \(err!.localizedDescription)")
            return
        }
        println("no errors")
    }
    
    func calendarWithName(name:String) -> EKCalendar? {
        //find if a calendar with name specified exists (I create this in anticipation for the final project)
        let calendars = self.database.calendarsForEntityType(EKEntityTypeEvent)as! [EKCalendar]
        //println(calendars)
        for cal in calendars {
            if cal.title == name {
                return cal
            }
        }
        self.createCalendar()//First time running ehhh
        return calendars[0]//Calendar we just made
    }
    
    
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        //hide keyboard when not in designated textbox :D
        self.view.endEditing(true)
    }

    
}
